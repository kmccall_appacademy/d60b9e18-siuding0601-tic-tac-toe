require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'
require 'byebug'

class Game

attr_accessor :player_one, :player_two, :board, :current_player

  def initialize(player_one, player_two)
    @player_one = player_one #human
    @player_two = player_two #computer
    @current_player = @player_one
    @board = Board.new
  end

  def play_turn
    board.place_mark(current_player.get_move, current_player.mark)
    # current_player.display(board)
    self.switch_players!
    current_player.display(board)
    puts "#{current_player.name}'s'turn"
  end


  def switch_players!
    if self.current_player == @player_one
      @current_player = @player_two
    else
      @current_player = @player_one
    end
  end


  def play

    until self.board.over?
      self.play_turn
    end
    if self.board.winner == player_one.mark
      puts "#{player_one.name} won"
    elsif self.board.winner == player_two.mark
      puts "#{player_two.name} won"
    else
      puts "No one wins"
    end
  end



end

game = Game.new(HumanPlayer.new("T"), ComputerPlayer.new("A"))
game.play
