class HumanPlayer

  attr_accessor :name, :mark

  def initialize(name = "Frankie", mark = :X)
    @name = name
    @mark = mark
  end

  def get_move
    puts "Where do you want to put it?"
    move = gets.chomp
    postions = move.delete(", ").split("")
    ans = []
    postions.each do |n|
      ans << n.to_i
    end
    ans
  end

  def display(board)
    p board.grid
  end
end
