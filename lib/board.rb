require 'byebug'
class Board
  attr_accessor :grid


  def initialize(grid=Array.new(3) {Array.new(3)})
    @grid = grid
  end

  def place_mark(array_of_pos, mark)
    self.grid[array_of_pos[0]][array_of_pos[-1]] = mark
  end

  def empty?(array_of_pos)
    if grid[array_of_pos[0]][array_of_pos[-1]] == nil
      return true
    else
      return false
    end
  end

  def winner
  # debugger
    #marks in row
    3.times do |i|
      mark = grid[i][0]
      count = 1
      2.times do |j|
        if grid[i][j+1] == mark
          count += 1
        end
      end
      if count == 3 && mark != nil
        return mark
      end
    end
    #marks in column
    3.times do |i|
      mark = grid[0][i]
      count = 1
      2.times do |j|
        if grid[j+1][i] == mark
          count += 1
        end
      end
      if count == 3 && mark != nil
        return mark
      end
    end
    #marks in left diagonal
      help_count = 1
    2.times do |i|
      mark = grid[0][0]
      if grid[i+1][i+1] == mark
        help_count += 1
      end
      if help_count == 3 && mark != nil
        return mark
      end
    end

    #marks in right diagonal
      help_count1 = 1
    1.upto(2) do |i|
      a = 0
      b = 2
      mark = grid[a][b]
      if grid[a+i][b-i] == mark
        help_count1 += 1
      end
      if help_count1 == 3 && mark != nil
        return mark
      end
    end
    return nil
  end

  def over?
    if grid.count([nil,nil,nil]) == 3
      return false
    elsif self.winner != nil
      return true
    elsif grid.join.split("").count < 8
      return false
    elsif self.winner == nil
      return true
    end


  end

end
