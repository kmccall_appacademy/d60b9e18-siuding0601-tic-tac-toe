class ComputerPlayer

  attr_accessor :name, :board ,:mark

  def initialize(name = "Bonzo", mark = :O)
    @name = name
    @board = board
    @mark = mark
  end

  def display(board)
    @board = board
  end

  def get_move
    grid = board.grid
    #mark move
    3.times.each do |i|
      if grid[i].count(mark) == 2 && grid[i].count(nil) == 1
        return [i, grid[i].index(nil)]
      end
    end
    #column move
    3.times do |i|
      count = 0
      blank = []
      3.times do |j|
        if grid[j][i] == mark
          count += 1
        elsif grid[j][i] == nil
          blank = [j, i]
        end
      end
      if count == 2
        return blank
      end
    end
    #random move

    a = rand(0..2)
    b = rand(0..2)
    while grid[a][b] != nil
      a = rand(0..2)
      b = rand(0..2)
    end
    return [a, b]
  end




end
